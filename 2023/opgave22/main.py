import string
import re
from pathlib import Path
from functools import lru_cache
from itertools import permutations
from collections import defaultdict
from datetime import datetime, timedelta


@lru_cache
def get_words() -> dict[int, set[str]]:
    words: dict[int, list[str]] = defaultdict(list)
    with (Path(__file__).parents[2] / 'data' / 'word_list_dutch.txt').open() as fid:
        pure_alpha = re.compile(pattern="^[A-Z]+$")
        for word in [line.rstrip().upper() for line in fid]:
            if not pure_alpha.match(word):
                continue
            words[len(word)].append(word)

    for k, v in words.items():
        words[k] = set(v)
    return words


@lru_cache
def get_word_constructs(word_length: int) -> dict[int, set[str]]:
    """Given the length of the word looking for, generate all unique combinations of 1 until word_length-1 characters"""
    word_constructs: dict[int, set[str]] = defaultdict(list)
    words_last_level = get_words()[word_length]
    for i in range(1, word_length)[::-1]:
        words_last_level = {w[:i] for w in words_last_level}
        word_constructs[i] = words_last_level
    return word_constructs


@lru_cache
def decode_char(char: str, delta: int) -> str:
    """For a given encoded character and a delta, return the decoded character"""
    index_char = string.ascii_uppercase.find(char)
    decoded_char_index: int = index_char - delta
    return string.ascii_uppercase[decoded_char_index]


def decode_word(word: str, delta: list[int]) -> str:
    """Decode a word given a list of delta using the caesar method"""

    if len(word) != len(delta):
        raise ValueError(f"To decode a word, the {delta=} should have equal length as the {word=}")

    return "".join(map(decode_char, word, delta))


def encode_word(word: str, delta: list[int]) -> str:
    encoded: str = ""
    for c, d in zip(word, delta):
        index_char = string.ascii_uppercase.find(c)
        decoded_char_index: int = index_char + d
        encoded = f"{encoded}{string.ascii_uppercase[decoded_char_index]}"
    print(f"Encoding {word:>12} > {encoded}")
    return encoded


def is_word(word: str) -> bool:
    """Check if the provided word is present in the word_list"""
    if word in get_words()[len(word)]:
        return True
    return False


def is_word_construct(word_length: int, word_construct: str) -> bool:
    """Validate if the provided word_construct is existing given the length of the decoded word"""
    if word_construct in get_word_constructs(word_length=word_length)[len(word_construct)]:
        return True
    return False


def force_decode_word(
        word: str, delta_options: list[int], extra_delta_options: list[tuple[str, set[int]]] = None,
        current_delta: list[int] = None, result: list[tuple[str, list[int]]] = None
):
    """recursive function to find existing words in a list based on caesar encryption.

    The function is optimised by using existing word_constructs
    """

    # Whenever we reach the 'end' of the function, check if the word is existing and if so, add to the results list
    if len(word) == len(current_delta or []):
        if (word := decode_word(word=word, delta=current_delta)) is not False:
            result.append((word, current_delta))
        return

    # Whenever we have not reached the end, let's validate if the given current_delta leads to an existing word_construct
    if current_delta is not None and not is_word_construct(
            word_length=len(word),
            word_construct=decode_word(word=word[: len(current_delta)], delta=current_delta),
            # Decode the word given the current_delta
    ):
        # If the current_delta does not lead to an existing word_construct we abandon this branch
        return

    # Building the different configurations based on the delta_options
    for d in delta_options:

        if extra_delta_options is not None and extra_delta_options[0][0] != '':
            # When the provided information is additive, only keep an option if it is listed
            if extra_delta_options[0][0] == '+' and d not in extra_delta_options[0][1]:
                continue
            # When the provided information is subtractive, only keep an option if it is not listed
            if extra_delta_options[0][0] == '-' and d in extra_delta_options[0][1]:
                continue

        updated_delta_options = delta_options.copy()
        updated_delta_options.remove(d)
        updated_current_delta = current_delta.copy() if current_delta is not None else []
        updated_current_delta.append(d)
        force_decode_word(
            word=word,
            delta_options=updated_delta_options,
            extra_delta_options=None if extra_delta_options is None else extra_delta_options[1:],
            current_delta=updated_current_delta,
            result=result,
        )
    return result


def group_options_per_delta_list(options: list[tuple[str, list[int]]], keep_order: bool = False) -> dict[
    set[int], list[str]]:
    to_return = defaultdict(list)
    for w, d in options:
        if not keep_order:
            d.sort()
        to_return[tuple(d)].append(w)
    for d, w in to_return.items():
        to_return[d] = list(set(w))
    return to_return


def force_decode_wordset(sentence: str, delta_options: list[int]):
    """This function is not yet working ... wip :)"""
    sentence: list[str] = sentence.split(" ")
    sentence_words = list(set(sentence))
    sentence_words.sort(key=lambda l: len(l), reverse=True)
    options = force_decode_word(word=sentence_words[0], delta_options=delta_options, result=[])

    file: Path = Path('output.txt')
    file.unlink(missing_ok=True)

    with file.open('w') as fid:

        # group options with same set of deltas (but different order)
        options = group_options_per_delta_list(options=options)
        for k, v in options.items():
            # Dit werkt niet - logica error
            if 'GEMARKEERDE' in v:
                print('GEMARKEERDE!')
                print(k)
            # Go over all the words provided and find a solution for each word given the set of delta_options
            # Only if for each extra_word at least 1 solution / option could be found, we have a possible solution
            memory: dict[str, list[str]] = defaultdict(list)
            for w in sentence_words:
                extra_word_options = force_decode_word(word=w, delta_options=list(k), result=[])
                if len(extra_word_options) == 0:
                    break
                memory[w] = list(set([i[0] for i in extra_word_options]))
            else:
                # When we reach this point, all words have found at least 1 solution
                fid.write(f"\n- Analysing the delta_options = {list(k)}\n")
                #print(f"\n- Analysing the delta_options = {list(k)}")
                for w in sentence:
                    fid.write(f"{w:<13} -> {memory[w]}\n")
                    #print(f"{w:<13} -> {memory[w]}")


def print_options(options, keep_order: bool = True) -> None:
    for k, v in group_options_per_delta_list(options, keep_order=keep_order).items():
        print(f"{','.join([str(i) for i in k]):>20} | {', '.join(v)}")


if __name__ == "__main__":
    start = datetime.now()

    # encode_word(word="DE", delta=[1, 3])
    # encode_word(word="VAKKEN", delta=[1, 3, 5, 2, 4, 6])
    # encode_word(word="HEBBEN", delta=[1, 3, 5, 2, 4, 6])
    # encode_word(word="EEN", delta=[1, 3, 5])
    # encode_word(word="GELE", delta=[1, 3, 5, 2])
    # encode_word(word="KLEUR", delta=[1, 3, 5, 4, 2])

    # force_decode_wordset(
    #    sentence="EH WDPMIT IHGDIT FHS HHQG LOJYT",
    #    delta_options=list(range(1, 9)),
    # )
    # print_options(force_decode_word(word="MITFTTMITLN", delta_options=list(range(1, 10)) * 2, result=[]))

    force_decode_wordset(
        sentence="HQ CE EX YF YQ GUYEQK XPNZ RUNS HYG YXFSEW SYCMXWFWR",
        delta_options=list(range(1, 27)),
    )
    print(stop)

    force_decode_wordset(
        sentence="HQ DNPEHYFNH CE EX YF WPEFB WCZFIF YQ KFPWSOSHJ GUYEQK XPNZ RUNS IJDEBG HYG PCPC YXFSEW SYCMXWFWR",
        delta_options=list(range(1, 27)),
    )
    print(stop)

    force_decode_wordset(
        sentence="",
        delta_options=list(range(1, 10)) * 2,
    )

    # options = force_decode_word(word="WDPMIT", delta_options=list(range(1, 7)), result=[])
    # options = force_decode_word(word="OJX", delta_options=list(range(1, 6)), result=[])
    # print(options)
    # options = force_decode_word(word="NCQMJX", delta_options=list(range(1, 7)), result=[])
    # print(options)
    # print(force_decode_word1(word='WDPMIT', delta_options=list(range(1,7))))

    # Test voor 'gemarkeerde' over 2 lijnen
    # options = force_decode_word(word="MITFTTMITLN", delta_options=list(range(1, 10)) * 2, result=[])
    # print(force_decode_word1(word='MITFTTMITLN', delta_options=list(range(1, 10)) * 2))

    # options = force_decode_word(word='GUYEQK', delta_options=list(range(1, 27)), result=[])
    # print(force_decode_word1(word='GUYEQK', delta_options=list(range(1, 10))))

    # options = force_decode_word(word='YXFSEW', delta_options=list(range(1, 27)), result=[])
    # options = force_decode_word(word='SYCMXWFWR', delta_options=list(range(1, 27)), result=[])

    print(f"The program took {(datetime.now() - start).total_seconds()}' to complete")
