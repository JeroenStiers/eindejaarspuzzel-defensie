from main import *


def encoding_test_set():
    """
        D E G E L E
        V A K K E N
    """
    encode_word(word="DE", delta=[1, 3])
    encode_word(word="GELE", delta=[2, 4, 5, 6])
    encode_word(word="VAKKEN", delta=[3, 4, 6, 2, 1, 5])
    # Results in 'EN IIQK YEQMFS'


def decoding_the_first_word():
    # Let's try with a random combination because we might be lucky :)
    print(decode_word(word="YEQMFS", delta=[1, 2, 3, 4, 5, 6]))

    # We are unlucky :/

    # Let's try multiple combinations and validate with the word_list
    # And assume we know that the delta options are 1-7
    options = force_decode_word(word="YEQMFS", delta_options=[1, 2, 3, 4, 5, 6, 7], result=[])

    # This is a bit hard to read so let's print them a bit nicer
    print("\nVisualising distinct options:")
    # This shows all different existing words based on the set of delta_options
    print_options(options, keep_order=False)

    # Hans told me that the set of delta_options is 1-6
    options = force_decode_word(word="YEQMFS", delta_options=[1, 2, 3, 4, 5, 6], result=[])
    print("\nVisualising all possible words with a given delta configuration")
    print_options(options)

    # Based on this list, we will assume that we are looking for 'VAKKEN' with a delta 3,4,6,2,1,5


def decoding_the_other_words():
    print("Trying to decode 'IIQK'")
    print_options(force_decode_word(
        word="IIQK",
        delta_options=[1, 2, 3, 4, 5, 6],
        extra_delta_options=[
            ('.'),  # Let's assume we don't know anything about the first character
            ('+', {4}),  # Let's assume we know the second character should be a 4
            ('-', {6}),  # The third character can be anything except for a 6
            ('+', {1, 6}),  # The fourth digit should be a 1 or a 6
        ],
        result=[]))

    # from this list, the only word that makes sense is clearly 'GELE'
    # Wich leaves for a pretty narrow search set for the last word
    print("Decoding 'EH'")
    print_options(force_decode_word(
        word="EH",
        delta_options=[1, 3],
        extra_delta_options=None,
        result=[]))


if __name__ == "__main__":
    # Create a test situation with the sentence "DE GELE VAKKEN"
    encoding_test_set()  # Results in 'EN IIQK YEQMFS'

    # We know that each word should consist of the 'delta' 1, 2, 3, 4, 5, 6
    # so we start with decoding the longest word
    decoding_the_first_word()

    # Given the choice of 'VAKKEN', we can solve the other two words
    decoding_the_other_words()
