import webbrowser
from itertools import product

base_url: str = f"http://maps.google.com/maps?z=17&t=m&q=loc:"
coordinates = (
    (19, 35, 40, 64, 43,),
    (30, 10, 55, 45, 42,),
    (25.60, 43.40, 14, 8.69, 40.20,),
    (147, 99, 47, 136, 7,),
    (20, 18, 58, 54, 9,),
    (31.30, 59.60, 44.30, 10.30, 25.70,),
)


def investigate_location(user_input: str):
    # user_input: str = "443111"
    signs = ("°", "'", '"', "°", "'", '"')
    selected_coordinates = [f"{coordinates[i][int(v[0])-1]}{v[1]}" for i, v in enumerate(zip(user_input, signs))]
    coordinate1 = "".join(selected_coordinates[:3])
    coordinate2 = "".join(selected_coordinates[-3:])

    for i, j in product(["N", "S"], ["E", "W"]):
        coordinate = f"{coordinate1}{i}+{coordinate2}{j}"
        url = f"{base_url}{coordinate}"
        print(coordinate)
        webbrowser.open(url)


if __name__ == "__main__":
    while True:
        print()
        investigate_location(user_input=input("Please provide a string of the 6 digits: "))
